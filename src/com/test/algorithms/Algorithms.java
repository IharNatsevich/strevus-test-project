package com.test.algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

import com.test.utils.CompareUtils;
import com.test.utils.Constants;

public class Algorithms {
	public static Stack<String> operationStack = new Stack<String>();
	public static List<String> outputString = new ArrayList<String>();

	private static void clearData(){
		operationStack.clear();
		outputString.clear();
	}
	
	public static Double calculate() {
		Double result = new Double(0);
				
		while (isOperationsExist()) {
			int index = getFirstOperationIndex();
			String op1 = outputString.get(index - 1);
			String op2 = outputString.get(index - 2);
			if (CompareUtils.isFunction(op1)) {

				result = calculateFunction(op1, op2);
				shiftOutputString(index, result, true);
				
			} else if (CompareUtils.isFunction(op2)) {

				result = calculateFunction(op2, op1);
				shiftOutputString(index, result, true);
				
			} else {

				result = calculateOperations(outputString.get(index), op2, op1);
				shiftOutputString(index, result, false);
				
			}
			
						
			System.out.println();
			for (String str1 : outputString) {
				System.out.print(str1);
			}
		}
		
		if (isFunctionExist() && !isOperationsExist()) {
			while (isFunctionExist()) {
				int index = getInsideFunctionIndex();
				String op1 = outputString.get(index);
				String op2 = outputString.get(index + 1);
				if (CompareUtils.isFunction(op1)) {
					result = calculateFunction(op1, op2);
					shiftOutputString(index+2, result, true);

				} else if (CompareUtils.isFunction(op2)) {

					result = calculateFunction(op2, op1);
					shiftOutputString(index+2, result, true);

				}
			}
		}

		return result;
	}

	
	private static void shiftOutputString(int index,Double result, boolean isFunctionCalc){
		
		if (!isFunctionCalc){
			outputString.remove(index);
		}
		outputString.remove(index - 1);
		outputString.remove(index - 2);
		outputString.add(index - 2, String.valueOf(result));

	}
	
	private static Double calculateOperations(String operation,
			String operator1, String operator2) {
		Double result = new Double(0);
		if ("+".equals(operation)) {
			result = Double.parseDouble(operator1)
					+ Double.parseDouble(operator2);
		}
		if ("-".equals(operation)) {
			result = Double.parseDouble(operator1)
					- Double.parseDouble(operator2);
		}
		if ("*".equals(operation)) {
			result = Double.parseDouble(operator1)
					* Double.parseDouble(operator2);
		}
		if ("/".equals(operation)) {
			result = Double.parseDouble(operator1)
					/ Double.parseDouble(operator2);
		}
		if ("^".equals(operation)) {
			result = Math.pow(Double.parseDouble(operator1),
					Double.parseDouble(operator2));
		}
		return result;
	}

	private static int getFirstOperationIndex() {
		int result = -1;
		for (int i = 0; i < outputString.size(); i++) {
			if (CompareUtils.isOperation(outputString.get(i))) {
				result = i;
				return result;
			}
		}
		return result;
	}
	
	private static int getInsideFunctionIndex() {
		int result = -1;
		for (int i = 0; i < outputString.size(); i++) {
			if (CompareUtils.isFunction(outputString.get(i))) {
				result = i;
			}
		}
		return result;
	}

	private static boolean isOperationsExist() {
		boolean result = false;
		for (String operation : Constants.operations) {
			for (String token : outputString) {
				if (operation.equals(token)) {
					result = true;
				}
			}

		}
		return result;
	}

	private static boolean isFunctionExist() {
		boolean result = false;
		for (String function : Constants.functions) {
			for (String token : outputString) {
				if (function.equals(token)) {
					result = true;
				}
			}

		}
		return result;
	}

	
	private static Double calculateFunction(String function, String argument) {
		Double result = new Double(0);
		if ("sqrt".equals(function)) {
			result = Math.sqrt(Double.parseDouble(argument));
		}
		if ("tg".equals(function)) {
			result = Math.tan(Double.parseDouble(argument));
		}
		if ("cos".equals(function)) {
			result = Math.cos(Double.parseDouble(argument));
		}
		if ("log".equals(function)) {
			result = Math.log(Double.parseDouble(argument));
		}
		if ("sin".equals(function)) {
			result = Math.sin(Double.parseDouble(argument));
		}

		return result;
	}

	public static void parseToRPN(String inputString) throws IllegalArgumentException{
	
		clearData();
		inputString = clearInputStr(inputString);

		StringTokenizer tokenizer = new StringTokenizer(inputString,
				Constants.createDelimeterStr(), true);

		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			System.out.println(token);

			if (CompareUtils.isNumber(token)) {
				outputString.add(token);
			} else if (CompareUtils.isFunction(token)) {

				outputString.add(token);
			} else if (CompareUtils.isOperation(token)) {

				while (!operationStack.isEmpty()
						&& CompareUtils.isOperation(operationStack
								.lastElement())
						&& Constants.priority.get(operationStack.lastElement()) >= Constants.priority
								.get(token)) {
					outputString.add(operationStack.pop());
				}
				operationStack.push(token);
			} else if (CompareUtils.isOpenBracket(token)) {

				operationStack.push(token);
			} else if (CompareUtils.isCloseBracket(token)) {

				while (!operationStack.isEmpty()
						&& !CompareUtils.isOpenBracket(operationStack
								.lastElement())) {
					outputString.add(operationStack.pop());
				}
				operationStack.pop();
			} else {
				 throw new IllegalArgumentException ("Incorrect input");
			}
		}
		
		while (!operationStack.isEmpty()) {
			outputString.add(operationStack.pop());

		}

	}

	private static String clearInputStr(String input){
		String result = input.replace(" ", "").replace("(-", "(0-")
	                .replace(",-", ",0-");
	        if (input.charAt(0) == '-') {
	            input = "0" + input;
	        }
	return result; 	
	}
}
