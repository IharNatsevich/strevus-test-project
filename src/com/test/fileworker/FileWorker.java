package com.test.fileworker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FileWorker {

	//private static String PATH = System.getProperty("user.dir")+"\\src\\";
	
	public static void write(String filePath, String output) {
		File file = new File(filePath);
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			PrintWriter out = new PrintWriter(new FileOutputStream(file.getAbsolutePath(),true));
			try {
				
					out.println(output);
						
			
			} finally {
				out.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static List<String> read(String filePath) throws FileNotFoundException {
		List<String> result = new ArrayList<String>();
		
		isFileExists(filePath);
		File file = new File(filePath);
			try {
				BufferedReader in = new BufferedReader(new FileReader(
						file.getAbsoluteFile())); 
				try {
					String s;
					while ((s = in.readLine()) != null) {
							result.add(s);
					}
				} finally {
					in.close();
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		
		return result;
	}
	
	
	private static void isFileExists(String fileName)
			throws FileNotFoundException {
		File file = new File(fileName);
		if (!file.exists()) {
			throw new FileNotFoundException(file.getName());
		}
	}
	
}
