package com.test.utils;

import java.util.HashMap;

public class Constants {
	public static HashMap<String, Integer> priority = new HashMap<String, Integer>() {
		{
			put("+", 1);
			put("-", 1);
			put("*", 2);
			put("/", 2);
			put("^", 3);
		}
	};

	public static String[] operations = { "+", "-", "/", "*", "^" };
	public static String[] functions = { "sqrt", "sin", "cos",
			"tg", "log" };
	public static String OpenBracket = "(";
	public static String CloseBracket = ")";

	public static String createDelimeterStr() {
		String str = OpenBracket + CloseBracket;
		for (int i = 0; i < operations.length; i++) {
			str += operations[i];
		}

		return str;

	}

}
