package com.test.utils;

public class CompareUtils {
	public static boolean isNumber(String token) {
		boolean result = true;
		try {
			Double.parseDouble(token);
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	public static boolean isFunction(String token) {
		boolean result = false;
		for (String function : Constants.functions) {
			if (token.equalsIgnoreCase(function)) {
				result = true;
			}

		}
		return result;
	}

	public static boolean isOperation(String token) {
		boolean result = false;
		for (String operation : Constants.operations) {
			if (token.equalsIgnoreCase(operation)) {
				result = true;
			}
		}
		return result;
	}

	public static boolean isOpenBracket(String token) {
		boolean result = false;
		if (token.equalsIgnoreCase(Constants.OpenBracket)) {
			result = true;
		}
		return result;
	}

	public static boolean isCloseBracket(String token) {
		boolean result = false;
		if (token.equalsIgnoreCase(Constants.CloseBracket)) {
			result = true;
		}
		return result;
	}

}
