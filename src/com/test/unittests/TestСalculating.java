package com.test.unittests;

import static org.junit.Assert.assertEquals;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tesp.app.CalcClass;
import com.test.algorithms.Algorithms;
import com.test.fileworker.FileWorker;

public class Test�alculating {

	private List<String> input = new ArrayList<String>();
	private Double[] result = { 6.0, 403.796753720054, 8.0, 1.0 };

	@Before
	public void readDataForCalcTest() {
		try {
			input = FileWorker.read(CalcClass.IN_FILE_PATH);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testCalculating() {
		for (String str : input) {
			try {
				Algorithms.parseToRPN(str);
				Double res = Algorithms.calculate();
				assertEquals(result[input.indexOf(str)], res);
			} catch (Exception e) {
				System.out.print(e.getMessage());
			}

		}
	}
	@Test
	public void testDivideByZero(){
		try{
			Algorithms.parseToRPN("12/sin(0)");
			
		} catch (Exception e){
			System.out.print(e.getMessage());
		}
		Double res  = Algorithms.calculate();
		assertEquals("Infinity", res.toString());
		
	}
	@Test (expected = NumberFormatException.class )
	public void testIncorrectInput(){
		try{
			Algorithms.parseToRPN("10 - ((cos(1");
		}catch (NumberFormatException e){
			System.out.print(e.getMessage());
		}
		
		
	}

}
