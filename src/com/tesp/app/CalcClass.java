package com.tesp.app;

import java.util.List;

import com.test.algorithms.Algorithms;
import com.test.fileworker.FileWorker;

public class CalcClass {
	
	public static String OUT_FILE_PATH = "D:\\workspace\\TestProj\\src\\testVarOutput";
	public static String IN_FILE_PATH = "D:\\workspace\\TestProj\\src\\testVars";
	
	public static void main(String[] args) {
		
		try {
			
			List<String> input = FileWorker.read(IN_FILE_PATH);
			for (String str : input) {
				try{
				Algorithms.parseToRPN(str);
				} catch(IllegalArgumentException e1) {
					System.out.println(e1.getMessage());
				}
				Double res = Algorithms.calculate();
				FileWorker.write(OUT_FILE_PATH, str+" = "+String.valueOf(res));
			}
			
			
			System.out.println("Done!");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		FileWorker.write(OUT_FILE_PATH, "\n");

	}

}
